class NodoB{
    constructor(nivel, nombre, valor, posicion, padre){
        this.nivel = nivel;
        this.nombre = nombre;
        this.valor = valor;
        this.posicion = posicion;
        this.padre = padre;
    }
    hijoI(nivel,nombre,  valor, posicion, padre){
        var nodo = new Nodo( nivel,nombre, valor, posicion, padre);
        return nodo;
    }
    hijoD(nivel,nombre, valor, posicion, padre){
        var nodo = new Nodo(nivel,nombre,  valor, posicion, padre);
        return nodo;
    }
}
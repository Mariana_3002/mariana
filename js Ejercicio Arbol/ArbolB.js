class ArbolB{
    constructor(){
        this.nodoPadre = this.agregarNodoPadre();
        this.bElemento = 17;
        this.bNodos = [];
        this.nivel = 1;
        this.cam = '';;
        this.sumar = 0;
    }
    agregarNodo(nivel,nombre,  valor, posicion, padre){
        var nodo = new NodoB(nivel,nombre,  valor, posicion, padre);
        return nodo;
    }
    agregarNodoPadre(nivel,nombre,  valor, posicion, padre){
        var nodo = new NodoB(nivel,nombre, valor, posicion, padre);
        return nodo;
    }

    VHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.bNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hijoI'))
            this.VHijos(nodo.hijoI);

        if(nodo.hasOwnProperty('hijoD'))
            this.VHijos(nodo.hijoD);

        return this.buscarNodos;

    }

    bValor(nodo){

        if(nodo.valor == this.bElemento)
            this.bElemento = nodo;

        if(nodo.hasOwnProperty('hijoI'))
            this.bValor(nodo.hijoI);

        if(nodo.hasOwnProperty('hijoD'))
            this.bValor(nodo.hijoD);

        return this.bElemento;
    }

    bCamino(nodo){

        if(nodo.padre != null){
            this.cam = this.cam + ' '+ nodo.padre.valor;
            this.bCamino(nodo.padre);
        }

        return this.bElemento.valor + ' ' + this.cam;
    }
    SumarCaminoN(nodo){
        
        
        if(nodo.padre != null){
            this.sumar = this.sumar + nodo.padre.valor;
            this.SumarCaminoN(nodo.padre);
           
        }
        return this.bElemento.valor + this.sumar;
    }

}


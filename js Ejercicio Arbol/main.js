var arbol = new ArbolB();
var buscarNd = [];
this.buscarNodos = [];

arbol.nodoPadre = arbol.agregarNodo( 0, "A" ,15,  "raiz", null);
arbol.nodoPadre.hijoI = arbol.agregarNodo(1, "B", 9, "hoja izquierda", arbol.nodoPadre);
arbol.nodoPadre.hijoD = arbol.agregarNodo(1, "C", 20, "hoja derecha", arbol.nodoPadre);
arbol.nodoPadre.hijoI.hijoI = arbol.agregarNodo(2, "D", 6,  "hoja izquierda", arbol.nodoPadre.hijoI);
arbol.nodoPadre.hijoI.hijoD = arbol.agregarNodo(2, "E", 14,  "hoja derecha", arbol.nodoPadre.hijoI );
arbol.nodoPadre.hijoI.hijoD.hijoI = arbol.agregarNodo(3, "F", 13, "hoja izquierda", arbol.nodoPadre.hijoI)
arbol.nodoPadre.hijoD.hijoI = arbol.agregarNodo(2, "G", 17,  "hoja izquierda", arbol.nodoPadre.hijoD);
arbol.nodoPadre.hijoD.hijoD = arbol.agregarNodo(2, "H", 64, "hoja derecha", arbol.nodoPadre.hijoD)
arbol.nodoPadre.hijoD.hijoD.hijoI = arbol.agregarNodo(3, "I", 26,  "hoja izquierda", arbol.nodoPadre.hijoD);
arbol.nodoPadre.hijoD.hijoD.hijoD = arbol.agregarNodo(3, "J", 72,  "hoja derecha", arbol.nodoPadre.hijoD)
console.log(arbol);

arbol.VHijos(arbol.nodoPadre);
console.log(arbol.bNodos);

var z =  arbol.bValor(arbol.nodoPadre);
console.log(z);

var caminoarbol = arbol.bCamino(z);
console.log(caminoarbol);

var sum = arbol.SumarCaminoN(z);
console.log(sum);